/*
 * Copyright 2022, Roberto Schiavelli Júnior                                   *
 * SPDX-License-Identifier: MIT                                                *
 */

import React from 'react'
import styles from './styles.module.scss'

export interface SectionTitleProps {
  title?: String
}

export interface SectionHeaderProps {
  children: React.ReactElement<SectionTitleProps>
}

export interface SectionProps {
  title?: String
  children?: React.ReactNode
}

function SectionTitle(props: SectionTitleProps) {
  return <h3 className={styles.section__title}>{props.title}</h3>
}

function SectionHeader(props: SectionHeaderProps) {
  return <header className={styles.section__header}>{props.children}</header>
}

function Section(props: SectionProps) {
  const title = props.title
  const children = props.children

  return (
    <section className={styles.section}>
      {title && title !== '' && (
        <SectionHeader>
          <SectionTitle title={title} />
        </SectionHeader>
      )}
      {children}
    </section>
  )
}

export default Object.assign(Section, {
  Header: SectionHeader,
  Title: SectionTitle
})
