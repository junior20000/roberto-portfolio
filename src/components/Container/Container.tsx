/*
 * Copyright 2022, Roberto Schiavelli Júnior
 * SPDX-License-Identifier: MIT
 */

import { ReactNode } from 'react'
import styles from './styles/Container.module.scss'

export type ContainerType = 'nav' | 'page' | 'page-content'

export interface ContainerProps {
  children: ReactNode
  type?: ContainerType
}

const typeMap = {
  nav: styles['container--nav'],
  page: styles['container--page'],
  'page-content': styles['container--page-content']
}

function Container(props: ContainerProps) {
  return (
    <div
      className={
        styles.container +
        ' ' +
        (props.type ? typeMap[props.type] : typeMap['page-content'])
      }
    >
      {props.children}
    </div>
  )
}

export default Container
