/*
 * Copyright 2022, Roberto Schiavelli Júnior
 * SPDX-License-Identifier: MIT
 */

import { useColorMode } from '@chakra-ui/react'
import React from 'react'
import GitlabIcon from './gitlab_icon'
import styles from './styles.module.scss'

export interface FooterProps {
  year: Number
  author: String
}

function Footer(props: FooterProps) {
  const { colorMode } = useColorMode()
  const isDark = colorMode === 'dark'

  return (
    <footer
      className={
        isDark ? styles.footer + ' ' + styles['footer--dark'] : styles.footer
      }
    >
      <div className={styles.footer__cr_container}>
        © {props.year} {props.author}. All Rights Reserved.
      </div>
      <a
        className={styles.footer__src_container}
        href="https://gitlab.com/junior20000/portifolio"
        target="_blank"
        rel="noopener noreferrer"
      >
        <GitlabIcon className={styles.footer__src_container__src_logo} />
        <p>Source code</p>
      </a>
    </footer>
  )
}

export default Footer
