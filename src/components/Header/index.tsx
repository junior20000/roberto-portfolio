/*
 * Copyright 2022, Roberto Schiavelli Júnior
 * SPDX-License-Identifier: MIT
 */

export { default } from './StripHeader'