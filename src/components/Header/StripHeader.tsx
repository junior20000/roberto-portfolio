/*
 * Copyright 2022, Roberto Schiavelli Júnior
 * SPDX-License-Identifier: MIT
 */

import Image from 'next/image'
import styles from './styles/StripHeader.module.scss'

export interface StripHeaderTitleProps {
  title: string
}

export interface StripHeaderEmblemProps {
  src: string
  alt: string
}

export interface StripHeaderProps {
  title: string
  subtitle?: string
  emblemSrc?: string
  emblemAlt?: string
}

function StripHeaderTitle(props: StripHeaderTitleProps) {
  return (
    <h2 className={styles['strip-header__data-container__title']}>
      {props.title}
    </h2>
  )
}

function StripHeaderEmblem(props: StripHeaderEmblemProps) {
  return (
    <Image
      className={styles['strip-header__emblem-container__emblem']}
      src={props.src}
      alt={props.alt}
      width="100px"
      height="100%"
      layout="responsive"
    />
  )
}

function StripHeader(props: StripHeaderProps) {
  const emblemAlt = props.emblemAlt ? props.emblemAlt : ""

  return (
    <>
      <div className={styles['strip-header']}>
        <div className={styles['strip-header__data-container']}>
          <StripHeaderTitle title={props.title} />
          <p>{props.subtitle}</p>
        </div>
        {props.emblemSrc && props.emblemAlt !== '' && (
          <div className={styles['strip-header__emblem-container']}>
            <StripHeaderEmblem src={props.emblemSrc} alt={emblemAlt} />
          </div>
        )}
      </div>
    </>
  )
}

export default Object.assign(StripHeader, {
  Title: StripHeaderTitle,
  Emblem: StripHeaderEmblem
})
