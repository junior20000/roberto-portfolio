/*
 * Copyright 2022, Roberto Schiavelli Júnior                                   *
 * SPDX-License-Identifier: MIT                                                *
 */

import Link from 'next/link'
import Image from 'next/image'
import { Text, useColorMode, useColorModeValue } from '@chakra-ui/react'
import styled from '@emotion/styled'

const logoSize = 20;

const LogoContainer = styled.span`
font-weight bold;
font-size: ${logoSize * 0.9}px;
display: inline-flex;
align-items: center;
justify-content: center;
align-content: center;
line-height: ${logoSize}px;
padding: 10px;
margin: 0;
overflow: visible;

& > span {
  overflow: visible !important;
}

& > span > span {
  overflow: visible !important;
}

&:hover img {
  -webkit-animation: bounce 0.7s cubic-bezier(0, 0, 1, 1) 0s both;
  animation: bounce 0.7s cubic-bezier(0, 0, 1, 1) 0s both;
}
`

const Logo = () => {
  const {colorMode, _} = useColorMode()

  const terminalImg = `/images/console-${colorMode}.png`

  return (
    <Link href="/">
      <a>
        <LogoContainer>
          <Image
            src={terminalImg}
            width={logoSize}
            height={logoSize}
            alt="logo"
          />
          <Text
            color={useColorModeValue('#212529', '#F8F9FA')}
            fontFamily='Open Sans'
            fontWeight="bold"
            ml={3}
          >
            Roberto Schiavelli Júnior
          </Text>
        </LogoContainer>
      </a>
    </Link>
  )
}

export default Logo
