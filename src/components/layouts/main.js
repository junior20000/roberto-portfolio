/*
 * Copyright 2022, Roberto Schiavelli Júnior                                   *
 * SPDX-License-Identifier: MIT                                                *
 */

import Head from 'next/head'
import { Box, Container } from '@chakra-ui/react'
import Navbar from '../navbar.js'
import Footer from '../Footer'

const Main = ({ children, router }) => {
  return (
    <Box as="main" height="inherit">
      <Head>
        <title>Roberto Schiavelli Júnior - Home</title>
        <meta
          name="description"
          content="Roberto Schiavelli Júnior's portifolio"
        />
        <meta name="author" content="Roberto Schiavelli Júnior" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
        <meta name="robots" content="index, follow" />
        <meta charSet="UTF-8"/>
      </Head>
      <Navbar path={router.asPath} />
      <Container maxW={'container.md'} pt={14} display={"flex"} flexDir={"column"} height={"100%"} alignItems={"center"}>
        {children}
        <Footer year={2022} author={'Roberto Schiavelli Júnior'}/>
      </Container>
    </Box>
  )
}

export default Main
