/*
 * Copyright 2022, Roberto Schiavelli Júnior                                   *
 * SPDX-License-Identifier: MIT                                                *
 */

import { Container, Box } from '@chakra-ui/react'
import { useColorModeValue } from '@chakra-ui/react'
import Section from '../components/Section'
import StripHeader from '../components/Header'

const Home = () => {
  return (
    <Container>
      <Box
        borderRadius="lg"
        mb={6}
        p={3}
        textAlign="center"
        bg={useColorModeValue('#DEE2E6', 'whiteAlpha.200')}
      >
        Hey there, I&apos;m a brazilian developer!
      </Box>
      <StripHeader
        title="Roberto Schiavelli Júnior"
        subtitle="Developer"
        emblemSrc={`/images/profile-picture.jpg`}
        emblemAlt="Roberto Schiavelli Júnior's profile picture"
      />
      <Section title="About me">
        <p>
          I am a developer passionate about technology and computers in general.
          Building things is what I love to do. When I am developing, nothing
          holds me back from having fun. From emulators to complex systems,
          there is nothing I can not build.
        </p>
      </Section>
    </Container>
  )
}

export default Home
