/*
 * Copyright 2022, Roberto Schiavelli Júnior                                   *
 * SPDX-License-Identifier: MIT                                                *
 */

import { ChakraProvider } from '@chakra-ui/provider'
import Layout from '../components/layouts/main'
import theme from '../libs/theme.js'
import '../../styles/global.scss'

const Site = ({ Component, pageProps, router }) => {
  return (
    <ChakraProvider theme={theme}>
      <Layout router={router}>
        <Component {...pageProps} key={router.route} />
      </Layout>
    </ChakraProvider>
  )
}

export default Site
