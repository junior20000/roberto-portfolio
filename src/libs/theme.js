/*
 * Copyright 2022, Roberto Schiavelli Júnior                                   *
 * SPDX-License-Identifier: MIT                                                *
 */

import { extendTheme } from '@chakra-ui/react'
import { mode } from '@chakra-ui/theme-tools'

const styles = {
  global: props => ({
    body: {
      bg: mode('#F8F9FA', '#23272A')(props)
    }
  })
}

const components = {
  // TODO: Component customization
}

const fonts = {
  heading: '"Open Sans"'
}

const colors = {
  water: '#CAF0F8'
}

const config = {
  initialColorMode: 'dark',
  useSystemColorMode: true
}

const theme = extendTheme({ config, styles, components, fonts, colors })
export default theme
