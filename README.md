# Roberto's Portfolio
> Just a minimalist portfolio (ﾉ◕ヮ◕)ﾉ*:･ﾟ✧

![Latest Release](https://badgen.net/gitlab/release/junior20000/roberto-portfolio "Latest Release")
![License](https://badgen.net/gitlab/license/junior20000/roberto-portfolio "License")
[![JavaScript](https://img.shields.io/badge/--F7DF1E?logo=javascript&logoColor=000 "JavaScript")](https://www.javascript.com/)
[![Visual Studio Code](https://img.shields.io/badge/--007ACC?logo=visual%20studio%20code&logoColor=ffffff "Visual Studio Code")](https://code.visualstudio.com/)
[![git](https://badgen.net/badge/icon/git?icon=git&label "GIT")](https://git-scm.com)
[![ESLint](https://img.shields.io/badge/eslint-3A33D1?style=for-the-badge&logo=eslint&logoColor=white "ESLint")](https://eslint.org/)
[![Prettier](https://img.shields.io/badge/prettier-1A2C34?style=for-the-badge&logo=prettier&logoColor=F7BA3E "Prettier")](https://prettier.io/)
[![LinkedIn](https://img.shields.io/badge/LinkedIn-0077B5?style=for-the-badge&logo=linkedin&logoColor=white "LinkedIn")](https://www.linkedin.com/in/roberto-schiavelli-j%C3%BAnior-86a3561a9/)

## Getting Started

### Prerequisites
> Make sure you have [Node.js](https://nodejs.org/) installed.

You may want to follow these instructions bellow in order to upgrade the **Node.js**, if already installed:

- npm: 
    - ***nix**: `npm install -g npm@latest`
    - **Win**: Download newest version through [here](https://nodejs.org/en/download/) or use [this](https://github.com/felixrieseberg/npm-windows-upgrade) command line tool

### Installation
1. Clone the repo:
    ```sh
    git clone https://gitlab.com/junior20000/roberto-portfolio.git
    ```
2. Install NPM packages:
    ```sh
    npm install
    ```
3. Run dev script:
    ```sh
    npm run dev
    ```

For more options, check the `scripts` section in `packages.json`.

## Built With
- [React](https://reactjs.org/) - A JavaScript library for building user interfaces.
- [Next.js](https://nextjs.org/) - A React framework with hybrid static & server rendering with TypeScript support, smart bundling, route pre-fetching, and more.
- [Chakra UI](https://chakra-ui.com/) - A simple, modular and accessible component library that gives you the building blocks you need to build your React applications.

## Contributing
This project is **NOT** opened to contribution.

## License
Distributed under the [MIT](https://opensource.org/licenses/MIT) License. See `LICENSE` for more information.